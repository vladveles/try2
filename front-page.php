<!DOCTYPE html>
<html>
<head>
    <meta charset="utf8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="animate.css">
    <link rel="stylesheet" href="normalize.css">
    <title>WOW.js + Animate.CSS</title>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.0/css/materialize.min.css">
    <script src="wow.min.js"></script>
    <script
        src="https://code.jquery.com/jquery-1.12.4.min.js"
        integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ="
    crossorigin="anonymous"></script>
    <script>
            new WOW().init();
    </script>
    <link rel="stylesheet" href="<?=get_template_directory_uri()?>/style.css">
</head>

<body class="body-body">

<img class="middle-image" src="<?=get_template_directory_uri()?>/images/bg2.gif">
<div id="primary">
    <header class="header1 clearfix">

        <div class="logo">
            <a href="#" class="custom-logo-link" rel="home" itemprop="url">
                <img class="" src="<?=get_template_directory_uri()?>/images/adara-logo.svg" alt="Adara Logo">
            </a>
        </div>

        <!--MainMenu-->
        <div class="menu-icon"><i class="material-icons">menu</i></div>
        <nav class="main-menu">
            <ul class="menu">
                <li><a href="#">about</a></li>
                <li><a href="#">solutions</a></li>
                <li><a href="#">mission</a></li>
                <li class="has-children">
                    <a href="#">case studies</a>
                    <ul class="sub-menu">
                        <li><a href="#">Case study #1</a></li>
                        <li><a href="#">Case study #2</a></li>
                        <li><a href="#">Case study #3</a></li>
                    </ul>
                </li>
                <li><a href="#">news</a></li>
                <li><a href="#">contact</a></li>
            </ul>
        </nav>
        <!--MainMenuEnd-->

        <!--MobileMenu-->
        <nav class="mobile-menu">
            <ul class="menu">
                <li><a href="#">about</a></li>
                <li><a href="#">solution</a></li>
                <li>
                    <a href="#">case-study</a>
                </li>
                <li><a href="#">news</a></li>
                <li><a href="#">contact</a></li>
            </ul>
        </nav><!--MobileMenuEnd-->
    </header> <!--HeaderEnd-->

    <div class="about-content">
        <div class="constellation-title">
            <h2>ADARA RESEARCH</h2>
        </div>

        <div class="intro-text-wrapper">
            <h3>Combining creativity with<br>
                discipline and statistical rigor</h3>
        </div>

        <div class="about-content-page-text">
            <h4>We help clients and agencies understand, build and develop their ideas.<br/>
                We use unique frameworks to help explain how their ideas and products are<br/>
                working and how they can be optimized further. </h4>
        </div>
        <div class="steps-wrapper">
            <i class="circle material-icons action-down front-page-action-button-down">arrow_downward</i>
        </div>

        <div class="entry-content">
            <h1 class="article-title">Who we are</h1>
            <p style="text-align: center;">ADARA Research is structured with the sole objective of meeting our clients’
                needs. We are nimble and flexible and can efficiently deliver on projects of any size and scope. We have
                no conflicting affiliations and no institutional bias toward any particular methodology or service.
                Instead, our only vested interest is delivering a solution that simply makes the most sense and is most
                cost effective for each client’s unique situation.</p>
            <p style="text-align: center;">We maintain partnerships with dozens of high-quality providers of every kind
                of research, design, and technology. We form project teams that are tailored to our client’s projects,
                so that we can use the tools and techniques that best fit each project and its unique issues. And we can
                do it in the right timeframe, at the right price, and with the highest level of quality.</p>
            <p style="text-align: center;">We have extensive experience conducting global studies and analyzing markets
                in Europe, Asia and Latin America. We often coordinate projects with local partners to ensure
                consistency of methodology and approach across markets.</p>

            <div class="row adjustable-elements adjustable-elements-3cols ">
                <div class="col s12 m12 l4">
                    <div>
                        <div class="icon-wrapper"><img src="<?=get_template_directory_uri()?>/images/SENIOR-TEAM.svg"></div>
                        <div class="title-wrapper" style="text-align: center;">SENIOR TEAM INVOLVEMENT</div>
                        <div class="content-wrapper" style="text-align: center;">Consultative team of practitioners heavily involved in the execution of all phases of research, providing strategic consulting, technical expertise, and seasoned perspective. We do not sell packaged products, but offer dynamic solutions and on-going support of senior staff.</div>
                    </div>
                </div>
                <div class="col s12 m12 l4" style="text-align: center;">
                    <div>
                        <div class="icon-wrapper"><img src="<?=get_template_directory_uri()?>/images/FLEXIBLE-METHODAGNOSTIC.svg"></div>
                        <div class="title-wrapper">FLEXIBLE, METHOD-<br>
                            AGNOSTIC APPROACH</div>
                        <div class="content-wrapper">Our broad-based research expertise lets us develop customized approaches that will best answer our clients’ business questions. We also believe in being flexible throughout the research process and adjusting our approach when necessary.</div>
                    </div>
                </div>
                <div class="col s12 m12 l4" style="text-align: center;">
                    <div>
                        <div class="icon-wrapper"><img src="<?=get_template_directory_uri()?>/images/DELIVERING-VALUE.svg"></div>
                        <div class="title-wrapper">DELIVERING VALUE</div>
                        <div class="content-wrapper">Our expertise allows us to work faster, smarter, and often more cost effectively. We offer more than sophisticated, high-end methods: we focus on clients’ business needs and understand how to design research that addresses multiple objectives.</div>
                    </div>
                </div>
            </div> <!--3colsEnd-->
            <p style="text-align: center;"><i class="circle material-icons action-down action-button-up">arrow_upward</i></p>
            <h2 style="text-align: center;">TEAM</h2>
        </div><!--entry-content-End-->

        <div class="row adjustable-elements adjustable-elements-4cols">
            <div class="col s12 m12 l3">
                <div>
                    <div class="icon-wrapper"><img src="<?=get_template_directory_uri()?>/images/TEAManna.jpg"></div>
                    <div class="title-wrapper">Anna Gurevich</div>
                    <div class="position-wrapper">Principal, Managing Partner</div>
                    <div class="content-wrapper">
                        <p>Anna is passionate about making ideas a reality; she leads a diverse team of professionals and encourages them to challenge each other and generate the best ideas. Drawing on her consulting background, Anna brings fresh and innovative perspective to better address clients’ business needs that span the product lifecycle, from identifying opportunities to growing brands. Anna holds a MA in Economics and Statistics.</p>
                        <p><strong>What item could you not live without?</strong><br>
                            My passport</p>
                    </div>
                </div>
            </div>
            <div class="col s12 m12 l3">
                <div>
                    <div class="icon-wrapper"><img src="<?=get_template_directory_uri()?>/images/TEAMmarina.jpg"></div>
                    <div class="title-wrapper">Marina Tararukhina</div>
                    <div class="position-wrapper">VP, Advanced Analytics Lead</div>
                    <div class="content-wrapper">
                        <p>Marina is a recognized marketing sciences expert with over 15 years of research and 10 years of academic experience. As an expert in conjoint analysis and predictive modeling she co-authored several publications in sociology and presented at national research conferences. Marina holds a MS in Math &amp; Mechanical Physics.</p>
                        <p><strong>What would your super power be?</strong><br>
                            A mind reader. Then again, life would be boring without any surprises.</p>
                    </div>
                </div>
            </div>
            <div class="col s12 m12 l3">
                <div>
                    <div class="icon-wrapper"><img src="<?=get_template_directory_uri()?>/images/TEAMtanya.jpg"></div>
                    <div class="title-wrapper">Tatiana Bilousova</div>
                    <div class="position-wrapper">Project Manager</div>
                    <div class="content-wrapper">
                        <p>Tanya is an insights expert with extensive CPG industry expertise and proven strength in applying quantitative and qualitative methodologies to address broad swath of Consumer and Shopper Insights issues. Tanya holds a MA in Psychology as well as MBA.</p>
                        <p><strong>Describe your perfect Sunday:</strong><br>
                            Long walk with my son and the dog on the beach, nice home-cooked dinner.</p>
                    </div>
                </div>
            </div>
            <div class="col s12 m12 l3">
                <div>
                    <div class="icon-wrapper"><img src="<?=get_template_directory_uri()?>/images/TEAMluba.jpg"></div>
                    <div class="title-wrapper">Luba Proger</div>
                    <div class="position-wrapper">Creative Director</div>
                    <div class="content-wrapper">
                        <p>Luba has a proven track record of delivering imaginative creative ideas. Over the past fifteen years she has developed branding strategies, product concepts, and educational and marketing materials. Luba has an extensive portfolio in branding, data visualization, web and print design. Luba is a founding director of Orange Fog Design Studio. Luba holds a BA in Graphic Design and Photography.</p>
                        <p><strong>What is your favorite hobby?</strong><br>
                            Dance</p>
                    </div>
                </div>
            </div>
        </div><!--adjustable-elements-4cols-->

        <h3 style="text-align: center;">We are always looking for talent</h3>
        <p style="text-align: center;"><a class="btn-primary" href="/contact/">Join Us</a></p>

     <footer class="page-footer">
        <div class="container">
            <div class="row">
                <div class="col l6 s12">
                    <aside id="secondary" class="sidebar widget-area" role="complementary">
                        <section id="text-2" class="widget widget_text">			<div class="textwidget"><a href="/" class="footer-logo-link" rel="home" itemprop="url"><img src="<?=get_template_directory_uri()?>/images/adara-logo.svg" alt="Adara Logo"></a>

                                <p class="copyright">© 2015 - 2017 ADARA Research. All rights reserved. <a href="/terms-conditions">Terms &amp; Conditions</a></p></div>
                        </section>                    </aside><!-- .sidebar .widget-area -->


                </div>
                <div class="col l2 m4 s12">
                    <div class="widget-area">
                        <section id="nav_menu-2" class="widget widget_nav_menu"><div class="menu-footer1-container"><ul id="menu-footer1" class="menu"><li id="menu-item-15765" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-5 current_page_item menu-item-15765"><a href="http://www.adara.calceron.com/">About</a></li>
                                    <li id="menu-item-15764" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-15764"><a href="http://www.adara.calceron.com/solutions/">Solutions</a></li>
                                    <li id="menu-item-15763" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-15763"><a href="http://www.adara.calceron.com/mission/">Mission</a></li>
                                </ul></div></section>                    </div><!-- .widget-area -->
                </div>
                <div class="col l2 m4 s12">
                    <div class="widget-area">
                        <section id="nav_menu-3" class="widget widget_nav_menu"><div class="menu-footer2-container"><ul id="menu-footer2" class="menu"><li id="menu-item-15768" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-15768"><a href="http://www.adara.calceron.com/case-studies/">Case Studies</a></li>
                                    <li id="menu-item-15767" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-15767"><a href="http://www.adara.calceron.com/news/">News</a></li>
                                    <li id="menu-item-15766" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-15766"><a href="http://www.adara.calceron.com/contact/">Contact</a></li>
                                </ul></div></section>                    </div><!-- .widget-area -->
                </div>
                <div class="col l2 m4 s12">
                    <div class="widget-area">
                        <section id="text-3" class="widget widget_text">			<div class="textwidget"><div class="social-icons">
                                    <a href=""><i class="fa fa-google-plus"></i></a>
                                    <a href=""><i class="fa fa-linkedin"></i></a>
                                </div>
                            </div>
                        </section>                    </div><!-- .widget-area -->
                </div>
            </div>
        </div>
     </footer>


    </div> <!--about-contentEnd-->
</div> <!--primaryEnd-->




<script>
    $('.menu-icon').click(function () {
        $('.mobile-menu').toggle("active");
    });
    $(window).resize(function () {
        if ( $(this).width() > 990 ){
            $('.mobile-menu').hide()
        }
    })
</script>

</body>
</html>